﻿using System.Collections.Generic;

namespace BookClass
{
    namespace BookClass
    {
        /// <summary>Book Author Comparator</summary>
        public class BookAuthorComparator : IComparer<Book>
        {
            /// <summary>Compare two books by author</summary>
            /// <returns>int</returns>
            public int Compare(Book firstBook, Book secondBook)
            {
                return firstBook.Author.CompareTo(secondBook.Author);
            }
        }
    }

}
