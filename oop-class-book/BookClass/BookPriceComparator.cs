﻿using System.Collections.Generic;

namespace BookClass
{
    /// <summary>Book Price Comparator</summary>
    public class BookPriceComparator
    {
        /// <summary>Compare two books by price</summary>
        /// <returns>int</returns>
        public int Compare(Book firstBook, Book secondBook)
        {
            return firstBook.Price.CompareTo(secondBook.Price);
        }
    }
}
